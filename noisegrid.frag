#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265358979323846
#define TWO_PI 6.283185307179586
#define CIRCLE(st,pos,size,blur)smoothstep(0.,blur/u_resolution.y,size-length(pos-st))
#define BUTTER PI/TWO_PI/PI

uniform vec2 u_resolution;

uniform sampler2D u_tex0;
uniform sampler2D u_tex1;
uniform sampler2D u_tex2;
uniform vec2 u_tex0Resolution;
uniform vec2 u_tex1Resolution;
uniform vec2 u_tex2Resolution;
uniform float u_time;

varying vec2 v_texcoord;
varying vec2 v2_texcoord;

float rand(vec2 c){
  return fract(sin(dot(c.xy,vec2(12.9898,78.233)))*99123.8293);
}

vec3 permute(vec3 x){return mod(((x*34.)+1.)*x,289.);}
float snoise(vec2 v){
  const vec4 C=vec4(.211324865405187,.366025403784439,
  -.577350269189626,.024390243902439);
  vec2 i=floor(v+dot(v,C.yy));
  vec2 x0=v-i+dot(i,C.xx);
  vec2 i1;
  i1=(x0.x>x0.y)?vec2(1.,0.):vec2(0.,1.);
  vec4 x12=x0.xyxy+C.xxzz;
  x12.xy-=i1;
  i=mod(i,289.);
  vec3 p=permute(permute(i.y+vec3(0.,i1.y,1.))
  +i.x+vec3(0.,i1.x,1.));
  vec3 m=max(.5-vec3(dot(x0,x0),dot(x12.xy,x12.xy),
  dot(x12.zw,x12.zw)),0.);
  m=m*m;
  m=m*m;
  vec3 x=2.*fract(p*C.www)-1.;
  vec3 h=abs(x)-.5;
  vec3 ox=floor(x+.5);
  vec3 a0=x-ox;
  m*=1.79284291400159-.85373472095314*(a0*a0+h*h);
  vec3 g;
  g.x=a0.x*x0.x+h.x*x0.y;
  g.yz=a0.yz*x12.xz+h.yz*x12.yw;
  return 130.*dot(m,g);
}


void main(){
  float r=rand(vec2(1000.));
  float n=snoise(vec2(v_texcoord.x,(v_texcoord.y*TWO_PI)+r+u_time*BUTTER*2.))*.5+.5; 
  vec2 uv=vec2(v_texcoord.x,v_texcoord.y);
  float amt = 500000.;
  uv.y+=snoise(uv+vec2(amt,-u_time*BUTTER*BUTTER));
  uv.x+=snoise(uv+vec2(amt,u_time*BUTTER*BUTTER));
  vec4 canvas=vec4(1.);
  vec4 color0=texture2D(u_tex0,fract(uv*2.));
  vec4 color1=texture2D(u_tex1,fract(uv*1.));
  
  gl_FragColor=vec4(color0);
}
